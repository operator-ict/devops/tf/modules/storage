resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

resource "azurerm_user_assigned_identity" "identity" {
  name                = "storage-account-identity-${var.name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags
}

resource "azurerm_storage_account" "sa" {
  account_kind             = "StorageV2"
  name                     = var.name
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = var.replication_type
  tags                     = var.tags
  is_hns_enabled           = var.is_hns_enabled

  blob_properties {
    delete_retention_policy {
      days = 7
    }
    container_delete_retention_policy {
      days = 7
    }
    dynamic "cors_rule" {
      for_each = var.cors_rule == null ? [] : ["X"]
      content {
        allowed_headers    = var.cors_rule.allowed_headers
        allowed_methods    = var.cors_rule.allowed_methods
        allowed_origins    = var.cors_rule.allowed_origins
        exposed_headers    = var.cors_rule.exposed_headers
        max_age_in_seconds = var.cors_rule.max_age_in_seconds
      }
    }
  }

  identity {
    type         = "UserAssigned"
    identity_ids = [azurerm_user_assigned_identity.identity.id]
  }
}

resource "azurerm_storage_container" "containers" {
  for_each              = var.containers
  name                  = each.key
  storage_account_name  = azurerm_storage_account.sa.name
  container_access_type = each.value.access_type
}

locals {
  privTypes = {
    rw = "Storage Blob Data Contributor",
    ro = "Storage Blob Data Reader"
  }
  privTuples = distinct(flatten(
    [
      for conatainerName, container in var.containers : [
        for privCode, roleName in local.privTypes : [
          for principal_id in coalesce(container[privCode], []) : [
            {
              conatainerName = conatainerName
              privCode       = privCode
              role           = roleName
              principal_id   = principal_id
            }
          ]
        ]
      ]
    ]
  ))
}
resource "azurerm_role_assignment" "roles" {
  for_each = {
    for p in local.privTuples : format("%s/%s/%s", p.conatainerName, p.privCode, p.principal_id) => p
  }
  scope                = azurerm_storage_container.containers[each.value.conatainerName].resource_manager_id
  role_definition_name = each.value.role
  principal_id         = each.value.principal_id
}

locals {
  storageAccountRoles = distinct([
    for p in local.privTuples : p.principal_id
  ])
}
# Je potreba mít alespon reader na Storage Account, abych se do něj dostal
resource "azurerm_role_assignment" "sa-roles" {
  for_each = toset(local.storageAccountRoles)
  scope                = azurerm_storage_account.sa.id
  role_definition_name = "Reader"
  principal_id         = each.value
}

resource "azurerm_storage_management_policy" "policies" {
  count              = length(coalesce(var.policies, {})) > 0 ? 1 : 0
  storage_account_id = azurerm_storage_account.sa.id

  dynamic "rule" {
    for_each = var.policies
    content {
      name    = rule.key
      enabled = true
      filters {
        prefix_match = rule.value.prefix_match
        blob_types   = ["blockBlob"]
      }
      actions {
        base_blob {
          tier_to_cool_after_days_since_modification_greater_than    = rule.value.tier_to_cool_after_days_since_modification_greater_than
          tier_to_archive_after_days_since_modification_greater_than = rule.value.tier_to_archive_after_days_since_modification_greater_than
          delete_after_days_since_modification_greater_than          = rule.value.delete_after_days_since_modification_greater_than
        }
      }
    }
  }
}

resource "azurerm_storage_table" "tables" {
  for_each             = toset(var.tables)
  name                 = each.value
  storage_account_name = azurerm_storage_account.sa.name
}
