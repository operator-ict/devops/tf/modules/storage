# Unreleased
## Added
## Changed
## Fixed

# [0.2.6] - 2025-01-03
## Added
- Add role Reader of Storage Account for all users with access to contaniers to see Storage Account

# [0.2.5] - 2024-09-18
## Added
- Add support of is_hns_enabled

# [0.2.4] - 2023-10-09
## Added
- Add storage tables

# [0.2.3] - 2023-06-21
## Fixed
- Removed appendBlob, because it is not compatible with tier_to_cool_after_days_since_modification_greater_than

# [0.2.2] - 2023-02-20
## Changed
- Setup policies and ro/rw roles as optional

# [0.2.1] - 2023-01-02
## Added
- Add tags

# [0.2.0] - 2022-11-07
## Added
- add lifecycle policy
## Changed
## Fixed