variable "name" {
  type = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "replication_type" {
  type = string
}

variable "containers" {
  type = map(object({
    access_type = string
    ro          = optional(list(string))
    rw          = optional(list(string))
  }))
}

variable "cors_rule" {
  type = object({
    allowed_headers    = list(string)
    allowed_methods    = list(string)
    allowed_origins    = list(string)
    exposed_headers    = list(string)
    max_age_in_seconds = number
  })
  default = null
}

variable "is_hns_enabled" {
  type    = bool
  default = false
}

variable "policies" {
  type = map(object({
    prefix_match                                               = list(string)
    tier_to_cool_after_days_since_modification_greater_than    = optional(number)
    tier_to_archive_after_days_since_modification_greater_than = optional(number)
    delete_after_days_since_modification_greater_than          = optional(number)
  }))
  default = {}
}

variable "tables" {
  type    = list(string)
  default = []
}
